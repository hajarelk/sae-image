 <p>Hajar EL Kasmi 
 <br>
BUT INFO
<br>
Année universitaire : 2022-2023
</p>




#  <div align="center"> SAE Images 
</div>

## Introduction 

<p> Dans le cadre de la SAE Images, nous étions chargé de manipulé des images , principalement des Images Encodés en BMP. 
<br>
Au fil de ce rapport je répondrais aux différentes questions auxquels nous devions répondre durant cette SAE afin de prouver
<br>
 la bonne comprehension et manipulation des Images BMP, de l'encodage, des indexations de couleurs.. 

_________________________________________

## Partie A0

***
<br>

1. Display affiche l'image mais aussi signale une erreur. Pourquoi ? 

<br>

<p>Il y a une erreur sur la taille du fichier.<br>
Quand on affiche la taille dans le terminal on trouve : 816026 OCTETS.<br>
Alors qu'a l'adresse 0X02 qui est censéé contenir la taille du fichier on trouve après la conversion en Hexa : 816025.
<br>
Il manque donc 1 octet sur le ficiers Okteta.
Afin de corriger cette erreur il faut ajouter  1 octet à l'adresse de la taille du fichier.
<br>
Il faut donc remplacer 0x99 par 0x9A.

![okteta](/saeiMG/A0/A0.png "erreur taille")


<br>

***
## Partie A1

***

<br>

1. Dans cet éditeur hexadecimal créez un nouveau fichier nommé : Image0.bmp :

<br>

### En-tête du Fichier

<p> À l'adresse : 0x00  sur 2 octet on a le type de fichier ici c'est BM=BItmap qui équivaut en hexa sur la table ASCII à 42 4D. <br>
À l'adresse 0X02 sur 4 octets il y a la taille totale du fichiers , nous la complèterons plus tard quand nous l'a connaîtrons.<br>
À l'adresse 0x06 et 0x08 sur 2 octets nous avons des champs reservés correspondant ici à 00 00 et 00 00.<br>
À l'adresse 0x06 et 0x08 sur 4 octets nous avons l'adresse de la zone de fefinition de l'image , ici le saut ets de 26 donc l'adresse est 1A 00 00 00.<br>

C'est tout pour l'En-tête du fichier.<br>

### En-tête du BMp

<br>

Ensuite pour la taille en octet de cette en-tête : elle correspond à l'adresse 0x0E => ici elle est de 12 octets en hexa cela correspond à 0x0C.<br>
Pour la Largeur de l'image en pixels : elle est de 4 : codé sur 4 octets : 04 00 00 00. <br>
Pour la Hauteur de l'image en pixels : elle est de 4 : codé sur 4 octets : 04 00 00 00. <br>
Le Plan de couleur est toujours de 01 : codé sur 2 octets à l'adresse 0x1A : 01 00.  <br>
Pour le Nombre de bits utilisé par pixel : elle est codé à l'adresse 0x1C sur 2 octets : 1 pixels contient 24 bit soit 3 octets.<br>
Étant donné que nous somme en RGB : chaque couleur est codé sur 1 octet : <br>

- R = 1 octet = 8 bits <br>
- G = 1 octet = 8 bits <br>
- B = 1 octet = 8 bits <br>

Pour coder les couleurs :<br>
Chaque case de notre image représente 1 pixels, en tout nous avons 16 cases sur notre image.<br>

*** Attention nous somme en RGB little Endian *** : donc on code de gauche à droite et du bas vers le haut. <br>
1 pixels = 24 bits -> R = 8 bits , G = 8 bits , B = 8 bits -> 8+8+8= 24 bits soit 3 octets.<br>

#### Représentation des couleurs : <br>

- Le Rouge représente ( 255 , 0, 0 ) = *** FF 00 00 en Hexa , Big Endian ***  et *** 00 00 FF en Hexa little Endian *** <br>
- Le Blanc représente (255, 255, 255) = *** FF FF FF en Big Endian et en little Endian *** <br>


- #### Ce qui nous donne le code suivant :
<br>

![damier code okteta](/saeiMG/A1/a1.png "code damier rouge") <br>
<br>

- #### Et voici le résultat final :

![damier](/saeiMG/A1/damierrougea1.png "damier")

<br>

Et enfin il ne faut pas oublié de calculer la taille du fichier en hexa et de l'ajouter à l'adresse 0x02 : <br>
Pour ce faire, il faut compter le nombre de bit dans le fichiers et le traduire en hexa, puis le coder sur 4 octets à l'adresse précédente.<br> 
Ici chaque ligne font 32 octets, j’ai 5 lignes = 160 + dernière ligne qui contient 2 octets = 162. <br>
Ce qui ce traduit en hexadécimal par A2. <br>


***

## Partie A2

***

<br>

1. Modifiez votre fichier pour obtenir cette image-là que vous nommerez Imagetest.bmp: <br>

On a besoin de plusieurs couleurs cette fois-ci : 

- Le bleu céruléen qui correspond à (15,157,232) en RGB, en hexadécimal cela donne : 0F 9D E8 et en *** little Endian : E8 9D 0F *** <br>
- Le vert qui correspond à (0,255,0) en RGB, en hexa cela donne : 00 FF 00 <br> 
- Le bleu qui correspond à (0,0,255) en RGB, en hexa cela donne : 00 00 FF -> on inverse car nous somme en *** little Endian : FF 00 00 *** <br>
- Le violet qui correspond à (255,0,255) en RGB, en  Hexa cela donne : FF 00 FF <br>
- Le bleu qui correspond à (0,255,255) en RGB, en hexa cela donne : 00 FF FF -> on inverse car nous somme en *** little Endian : FF FF 00 *** <br>
- Le Blanc et le rouge nous le connaissons déjà. <br>

On code toujours du bas vers le haut et de la Gauche vers la droite : <br>
On commence donc par codé le Cyan, puis le violet , le bleu céruléen , le blanc etc.. <br>

#### Voici le code et le résultat Final : 

<br>

![imagetestcode](/saeiMG/A2/questiona2code.png "imagetestcode")
<br>

![imagetest](/saeiMG/A2/questiona2.png "imagetest")

<br>

***

## Partie A3
***

<br>

1. L'image Image0.bmp a un poids de 74 octets. Si on la convertit en format Windows NT, 3.1x alors le BITMAPCOREHEADER de 12 octets est remplacé par un BITMAPINFOHEADER de 40 octets (voir ici). <br>

Normalement le nombre d’octet sera de 102 étant donné que auparavant il y avait 12 octets et maintenant il y en a 40. <br>
- 40-12+74 = 102  <br>

- On peut voir que en convertissant l’image, la taille du fichier est de 66 en hexa c'est à dire a102 en déc.<br>


2. Quelle est le poids alors de notre image? <br>

Le poids de notre images est indiqué à l'adresse 0x02, ce qui correspond à 66 soit 120 octets.<br>

3. Combien y-a-t-il de bits par pixel? <br>

Il y a 24 bits par pixels soit 3 octets par pixels, comme nous l'avions expliquer précèdement. Cependant on peut également le voir à l'adresse 0x1C qui correspond au nombre de bits par pixels. Cette adresse correspond à 0x18 ce qui correspond à 24 en décimal soit 24 bits / pixels. <br>

4. Quelle est la taille des données pixels? <br>

- Concernant le nombre de données pixels : elle est indiquée à l’adresse ci-dessous sur 4 octets (0x22) : <br>

![adresse0x22](/saeiMG/A3/Nb%20donn%C3%A9e%20pixels.png "adresse0x22")

<br>

Elle est donc de 30 hexadecimal ce qui correspond à 48 octets par pixels. <br>
On peut aussi la calculer en faisant 4x4 = 16 = nombres de pixels sur l'image puis  multiplie le résultat par 3 car chaque pixels est codé sur 3 octets = 48. <br>



5. Y a-t-il une compression utilisée? <br>

Ici il n’y a rien donc il n’y a pas de compression. <br>
Nous pouvons voir cela à l'adresse 0x1E , lorsqu'il n'y a pas de compression tout est à 0. <br>

![compression](/saeiMG/A3/Compression.png "compression")

<br>
   
6.  Le codage des pixels a-t-il changé? <br>

On peut voir ci dessous que le codage des pixels n’a pas changé.<br>
- On a toujours 3 octets par pixels. <br>
- Entourer en bleu sur l'image ci-dessous, on peut voir qu'il y a en tou 16 pixels comme précédement.<br>

![codagecouleurs](/saeiMG/A3/Codage%20change.png "codagesdespixels")

<br>

***

## Partie A4 

***

1. Combien y-a-t-il de bits par pixel ? <br>

Sur le code de l’image.2 on peut remarquer, qu’à l’adresse 0x1C qui indique le nombre de bit par pixels il y a le chiffre 01 00.<br>
Il y a donc 1 bit par pixels. <br>


2. Quelle est la taille des données pixels ? <br>

La taille des données pixels , elle est indiquée à l’adresse (0x22) sur 4 octets : 10 en hexadécimal qui correspond à 16. <br>
La tailles des donnes pixels est donc de 16 octets . <br>

![codageimage2](/saeiMG/A4/Q2.png"codageimage2")

<br>


3. Y a-t-il une compression utilisée? <br>

Il n’y a toujours pas de compression utilisée car à l’adresse 0x2E qui correspond au type de compression,<br> 
on a la valeur 0 qui signifie qu’il n’y a pas de compression . <br>

![codageimg2compresion](/saeiMG/A4/Q3.png "codageimg2compresion")

<br>


4. Comment sont codées les couleurs de la palette ? <br>

On remarque que les couleurs de la palette sont codé sur 4 octets : <br>
1 octet pour le bleu , 1 pour le vert et 1 pour le rouge.  <br>
Et un dernier octet réservé. <br> 
Ceci correspond au codage en RGBA32. <br>
<br>

![codageimg2](/saeiMG/A4/Q4.png "codageimg2")

<br>


5. Quel est le nombre de couleurs dans la palette ? <br>

Le nombre de couleurs dans la pailletés est de 2 on peut le voir à l’adresse 0x2E : <br>

![img2couleurs](/saeiMG/A4/Q5.png"img2couleurs")

<br>

Il y a donc deux couleurs dans l’images le blanc et le rouges. <br>
Cela indique également qu’une palette est utilisée.<br>
Ainsi dans cette palette nous utilisons deux couleurs. <br>
 


6. Le codage des pixels a-t-il changé? (notez que les pixels sont à présent dans un bloc de 32 bits <br>


En soit le codage a changé oui et non : <br>

Oui car on n'encode plus pixels par pixels mais on encode par couleurs, cette fois-ci les deux couleurs sont représenté ( le blanc et le rouge ) <br>
Cepdendant nous encodons toujours les couleurs de 00 à FF on ajoute seulement 1 octet reservé à la fin de celle-ci . <br>
Le ruoge correspond toujours à  00 00 FF (00) <br>
Et le blanc correspond toujours à FF FF FF (00). <br>
Cependant nous ne codons plus en RGB mais en RGBA32 <br>

![img2couleurs](/saeiMG/A4/Q6.png"img2couleurs")

<br>


7. Changez la couleur rouge des pixels en bleu pour obtenir l'image ci-dessous que vous nommerez ImageBleue.bmp.<br>

Pour faire cela : 

Le Bleue équivaut en little Endian RGBA à : FF 00 00 (00).<br>
Le blanc reste le même.<br>

J’ai ensuite remplacé l'encodage des pixels rouges codés sous la forme de 00 00  FF 00  par l'encodage bleue. <br>
- *** Voici le résultat : *** <br>

![damiersbleue](/saeiMG/A4/Q7.png"damiersbleue")

<br>


![damiersbleue](/saeiMG/A4/Q7B.png"damiersbleue")

<br>

8. Inversez le damier : les blancs à la place des bleus et les bleus à la place des blancs, pour obtenir l'image ci-dessous.<br>

Pour ce faire : j’ai juste inverse le bleue et le blanc dans mon code. <br>
- J’ai remplacé le FF 00 00 00 - FF FF FF 00. <br 
- Par FF FF FF 00  - FF 00 00 00. <br>


![damiersbleueinverse](/saeiMG/A4/Q8.png"damiersbleueinverse")

<br>  

![damiersbleueinverse](/saeiMG/A4/Q8B.png"damiersbleueinverse")

<br>  

9. Modifiez le fichier  en mode index de couleurs avec okteta de façon à obtenir ceci. Enregistrez cette image sous ce nom Image3.bmp:<br>

J’ai raisonné par tâtonnement.<br>

J’ai compris en faisant plusieurs teste et modification sur mon encodage que une ligne correspondais à 1 octet. <br>
En tout on a 4 octet 
Le premiere octet correspond à la quatrième ligne tout en bas de l'image.
Le deuxième octet correspond à la troisième ligne etc.. <br>

 J'ai remarqué que seul la premiere valeur de l'octet est importante. <br>
Tout va de 5 en 5 : <br>
- 00 => j’ai une ligne en rouge. <br>
- 50 => j’ai un damier qui commence par la couleur rouge. <br>
- A0 => 10  j’ai un damier qui commence par la couleur blanche. <br>
- F0 => 15 j’ai une ligne blanche. <br>



10. Passez le fichier de l'ancien logo du Département d'Informatique en mode index de couleurs: <br>

Voici le code généré : <br>

![codeindex](/saeiMG/A4/Q10.png"codeindex")

<br>

11. A quelle adresse peut-on trouver le nombre de couleurs qu'il y a dans la palette? <br>

C’est à l’adresse 00x2E que l’on va retrouver la nombre de couleurs dans l’images. Ici il y a 16 couleurs . <br>
A cette adresse on à effectivement 10 hexadécimal = 16 en decimal. Soit 16 couleurs dans la palette <br>

![codepalette](/saeiMG/A4/Q11.png"codepalette")

<br>


12. A quelle adresse dans la palette peut-on trouver la couleur à dominante "Blanc" utilisée par cette image?  Attention il y a souvent des nuances de blanc. Pour vous aider et tester les couleurs codées en RGBA vous pouvez utiliser ce site web là: ICI.  <br>

On peut  retrouver cette couleur à dominante blanche a l’adresse : 00x76. <br>
C’est la couleur CC CC CC CC = en RGB 204,204,204.<br>
C’est la couleur que l’on retrouve majoritairement sur l’image. <br>

![imgq12](/saeiMG/A4/Q12.png"img12")

<br>

13. Où commence le tableau de pixel? <br>

Le tableau des pixels commence : <br> 
 
Commence a l’adresse 0x76. <br>


![imgq13](/saeiMG/A4/Q13B.png"img13")

<br>


14. En modifiant l'Hex,  placez quelques pixels bleus tout en bas de l'image. Pour obtenir en bas à gauche de l'image ceci (visualisez en utilisant Gimp par exemple) <br>

Pour ce faire : <br>

Comme les pixels sont coder de gauche à droite et du bas vers le haut. <br> 
Je me suis placé sur le 1er pixels ce situant  à l’adresse  0X76  et je l’ai  remplacé par une teinte de bleue qui correspond au E. <br>


![imgq14](/saeiMG/A4/Q14.png"img14")

<br>

Voici mon code après avoir effectuer les modifications : <br>
Encadrer en jaune ce sont tous les pixels que j'ai remplacé:<br>


![imgq14](/saeiMG/A4/Q14B.png"img14")

<br>

![imgq14](/saeiMG/A4/Q14C.png"img14")

<br>


15. Que se passe-t-il si l'on diminue le nombre de couleurs dans la palette? Que se passe t-il d'un point de vue visuel? Et dans l'hexa? <br>

Voici le avant après : <br>

![imgq15](/saeiMG/A4/Q15.png"img15")

<br>

![imgq15](/saeiMG/A4/Q15B.png"img15")

<br>

- ### D'un point de vue visuel : <br>

![imgq15](/saeiMG/A4/Q15C.png"img15")

<br>

- On peut voir que les couleurs sont beaucoup plus terne , à certain endroit on tire même vers le noir, notamment vers les bord droit du cercle ( entourer en rouge). <br>
- Le orange sur le logo à complètement disparu. <br>
- Sur la parti en haut à gauche on peut voir des troues, certains pixels sont sans couleurs. <br>
- Les pixels manquants ce voit à l’œil nu. <br>
- Les écritures sont également moins visibles. <br>
- Tout ceci rend l’image  beaucoup moins net. <br>

- ### Du point de vue de l'Hexa : <br>

![Ancien encodage](/saeiMG/A4/Q15E.png"Ancien encodage")

<br>

![nouvelle encodage](/saeiMG/A4/Q15D.png"nouvelle encodage")

<br>

- Tout d'abord on peut remarquer que toute les lettes on disparue , on ne voit plus que des 11 11 et des  000000 . <br>
- Étrangement on peu voit que même si à l’œil nu voit qu'il manque des couleurs  et bien le nombre de couleurs de la palette qui ce trouve à l’adresse 0x2E n’a pas changé.<br> - Il y a toujours 16 couleurs dans la palettes, il n'y a pas eu de diminutions contrairement à ce que l'on aurait pu penser.<br>
- Cepdendant on peut voir que dans le tableau de pixels plusieurs pixels on été changer :<br>
-CC CC CC CC est maintenant ^résent sous la forme de 11 11 11 11 par exemple. <br>


***
## Partie A5

***

- J'ai testé plusieurs chose mais je n'ai pas reussie a trouver la solution.<br>
- Mon problème principale était de savoir comment coder le -04 à l'adresse 0x16 qui correspond à la hauteur de l'image.<br>
- J'ai essayé de convertir -4 en C2, en signé etc.. mais rien en fonctionner.<br>
- Cependant après fait de nombreuse recherche j'ai compris qu'inverser le nombre permettait de lire l'image à l'envers et donc de la retourner.<br>

***
## Partie A6

***

1. Quel est le poids du fichier? Pourquoi? Que c'est-il passé? <br>

Le poids de l’image c’est :
Le nombre d’octet par pixel x le nombre de pixels. <br>
On a 16 pixels pour l’image 0 avec 3 octet par pixels = 48. <br>

Le poids du nouveau fichiers est de 16 octet. <br>
- Car on a toujours une image de dimension 4x4 comme on peut le voir au adresse : 0x12 et 0x16. <br>
Cependant à l’adresse 0xC1 on à le nombre de bits par pixels , il n’est plus de 24 bits par pixels mais de 1 bits par pixels . <br>
Donc 1 pixel est représenté sur 1 octet : ce qui nous donne 4x4x1 = 16 . <br>

![q1](/saeiMG/A6/Q1.png"1")

<br>

- Que c'est t'il passé ? <br>

On peut voir qu’il a compressé le nombre de répétition des pixels codés <br> 
Voici - le code de l'image précédente : on peut voir que chacun des 16 pixels est codés soit en rouge soit en blanc :<br>

![code image précedente](/saeiMG/A6/Q1A.png"code de l'image précedente")

<br>

- Et voici le code actuelle : <br>
Ou l'on a seulement la représentation du rouge une seule fois par : 00 00 FF (00). <br>
Et le blanc est également représenter une seul fois dans tout l'encodage par : FF FFFF (00). <br>

![image actuelle](/saeiMG/A6/Q1B.png"image actuelle")

<br>

- D’ailleurs on peut remarquer qu’il y a compression à l’adresse 0X1E qui est dorénavant de  00.01.00.08. (encadré en bleue sur l'image) <br> 


2. Trouvez dans l'entête l'offset que donne l'adresse de début des pixels. <br>


![image actuelle](/saeiMG/A6/Q2.png"image actuelle")

<br>

- Celle -ci ce trouve à l’adresse 0X0A:  36 04 00 00. <br>

-> *** La compression RLE *** : parcours les données , repères celle qui ce répètent et signal le nombre de fois ou ces données ce répètent : <br>

-Exemple: PPRPERRE RLE cela donne 2P 1R 1P 1E 2R. <br>


3. Décodez le code des pixels. (C'est-a-dire essayez de retrouver dans l'hexadécimal  le codage des pixels et expliquez-le) <br>

![image actuelle](/saeiMG/A6/Q3.png"image actuelle")

<br>

- On peut remarquer à l’adresse 0X36 que l’on code seulement le rouge et le blanc en 1 seul fois et non 8 fois chacun comme précédemment car nous avons 8 pixels blanc et 8 pixels rouge dans notre image. <br>

De plus on peut remarquer qu’ici nous considèrons qu’il n’y a qu’une seul couleur dans l’image , ainsi qu’une seul couleur importante qui ets probablement le rouge <br>
On peut voir ceci à l’adresse : 0x2E et 0x32.<br>

![image actuelle](/saeiMG/A6/Q3B.png"image actuelle")

<br>

*** 
## Partie A7 

***


1. Quel est le poids du fichier Image5.bmp? Pourquoi est-il moins grand que celui de l'image Image4.bmp? <br>

Le poids de l’image5 : soit on prend la taille général de l’image situé à l’adresse 0x02 qui est de 00 00 04 4E et qui correspond à 590 octets. <br>
Ou alors on prend la taille des données image situées à l’adresse 0X22 et qui correspond à : 18 hexadecimal donc 24 octets.

L’image4 faisait 1120 octets ( = tailles dû fichiers totale).<br>
Et sa taille des données image faisait 42 octets. <br 


- Soit entre l’image4 et l’image5 on a une différence de poids qui est double. <br>
- Ceci est probablement du au faite que l’image5 est codé en index de couleurs et non pixels par pixels ce qui fait gagner des octets. <br>

2. Décodez le code des pixels. (C'est-a-dire essayez de retrouver dans l'hexadécimal  le codage des pixels et expliquez-le) <br>


![image actuelle](/saeiMG/A7/1.png"image actuelle")

<br>

C’est quasiment le même code que précédemment ce qui diffère c’est la raille des donnes image et la taille du fichiers qui est plus petit ici étant donner que l’on code l’image par indexation.<br>

- Quand on descend tout en bas on a des valeurs soit 01 , 00 ou 04 . c'est ceci qui permet de déteriner qu'elle ligne colorer et qu'elle pixels colorés. <br>


![image actuelle](/saeiMG/A7/a72.bmp"image actuelle")

<br>

***
## Partie A8 
***

- J’ai découvert qu toute la parti encadrer en bleu. Servait a coder les 4 lignes de l’image. <nr>
- 1 ligne represente 4 octet : 00 00 04 01 permet de coder la derniere ligne en blanc <br>
- 00 00 04 00 permet de coder la ligne du dessus en rouge ligne en rouge. <br>


![image actuelle](/saeiMG/A8/Q1.png"image actuelle")

<br>

- Cependant je n'ai pas compris comment reusir à récuperer l'image final malgré de nombreux essaies.<br>






